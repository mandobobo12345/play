// ignore_for_file: unnecessary_null_comparison, unused_local_variable

import 'package:flutter_test/flutter_test.dart';
import 'package:play/pages/current_movie/screens/widgets/actors_widget.dart';

void main() {
  testWidgets("Test Widget", (tester) async {
    await tester.pumpWidget(const ActorsWidget());

    final titleFinder = find.byWidget(const CustomListViewActor());

    await tester.tap(titleFinder);

    await tester.pump();

    expect(titleFinder, findsOneWidget);
  });
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/services/service_get_it.dart';
import '../../../core/utils/app_constance/app_constance.dart';

import '../controller/intersting_movie_bloc.dart';

import '../controller/intersting_movie_event.dart';
import '../controller/intersting_movie_state.dart';
import 'widgets/custom_failure_intersting_movie_screen.dart';
import 'widgets/custom_loading_intersting_movie_screen.dart';
import 'widgets/loaded_widegt.dart';

class InterstingMovieScreen extends StatelessWidget {
  const InterstingMovieScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 10.0),
          child: BlocProvider(
            create: (context) => sl<InterstingMovieBloc>()
              ..add(
                LoadGetInterstingMovieEvent(),
              ),
            child: BlocConsumer<InterstingMovieBloc, InterstingMovieState>(
              listener: (context, state) {},
              builder: (context, state) {
                switch (state.request) {
                  case RequestSatuts.empty:
                    return const Text("data");
                  case RequestSatuts.loading:
                    return const InterstingMovieLoadingStatuWidget();
                  case RequestSatuts.loaded:
                    final bloc = BlocProvider.of<InterstingMovieBloc>(context);
                    return LoadedWidget(
                      state: state,
                      media: media,
                      bloc: bloc,
                    );
                  case RequestSatuts.failure:
                    final bloc = BlocProvider.of<InterstingMovieBloc>(context);
                    return RefreshIndicator(
                      onRefresh: () async {
                        bloc.add(LoadGetInterstingMovieEvent());
                      },
                      child: ListView(
                        children: const [
                          InterstingMovieFailureStatueWidget(),
                        ],
                      ),
                    );
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}

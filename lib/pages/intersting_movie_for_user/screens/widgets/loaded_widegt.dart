import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../../bottom_navigation_bar.dart';
import '../../../../core/text_styles/text_style.dart';
import '../../../../core/utils/api_consatance.dart';
import '../../../../core/utils/app_constance/color_constance.dart';
import '../../../../core/utils/app_constance/icon_constance.dart';
import '../../../../core/widgets/custom_button.dart';
import '../../../../core/widgets/custom_navigation.dart';
import '../../../../main.dart';
import '../../controller/intersting_movie_bloc.dart';
import '../../controller/intersting_movie_event.dart';
import '../../controller/intersting_movie_state.dart';

class LoadedWidget extends StatelessWidget {
  final InterstingMovieState state;
  final Size media;
  final InterstingMovieBloc bloc;
  const LoadedWidget({
    super.key,
    required this.state,
    required this.media,
    required this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Positioned.fill(
        child: CustomScrollView(
          shrinkWrap: true,
          slivers: [
            const SliverToBoxAdapter(
              child: SizedBox(height: 30),
            ),
            SliverToBoxAdapter(
              child: Text(
                "Choose 3 or more shows that interest you",
                style: largeStyleSize.copyWith(
                  fontSize: 35,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ),
            const SliverToBoxAdapter(child: SizedBox(height: 15)),
            SliverToBoxAdapter(
              child: GridView.builder(
                scrollDirection: Axis.vertical,
                physics: const NeverScrollableScrollPhysics(),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: 3 / 4.5,
                  crossAxisSpacing: 5,
                  mainAxisSpacing: 10,
                  crossAxisCount: 3,
                ),
                itemCount: state.interstingMovieModel!.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return InkWell(
                    borderRadius: BorderRadius.circular(50),
                    onTap: () {
                      bloc.add(SelectItemEvent(index: index));
                    },
                    child: AnimatedSize(
                      duration: const Duration(milliseconds: 1500),
                      curve: Curves.linear,
                      reverseDuration: const Duration(milliseconds: 1500),
                      child: Container(
                        width: 200,
                        height: 600,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Stack(
                          children: [
                            Positioned.fill(
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: CachedNetworkImage(
                                  width: 100,
                                  height: 200,
                                  progressIndicatorBuilder:
                                      (context, url, progress) {
                                    return Center(
                                      child: CircularProgressIndicator(
                                        strokeWidth: 2,
                                        color: ColorConstance.cDefaultColor,
                                        backgroundColor: ColorConstance
                                            .cDefaultColor
                                            .withOpacity(.05),
                                      ),
                                    );
                                  },
                                  imageUrl: ApiConstance.imageUrl(
                                    state.interstingMovieModel![index]
                                        .posterPath!,
                                  ),
                                  fadeInCurve: Curves.bounceInOut,
                                  fit: BoxFit.fill,
                                  errorWidget: (context, url, error) => Center(
                                    child: Image.asset(
                                      IconConstance.iWarningIcon,
                                      height: 30,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            for (int i = 0;
                                i < state.interstMovieList.length;
                                i++) ...[
                              if (state.interstMovieList.elementAt(i) ==
                                  state.interstingMovieModel![index].id) ...[
                                Positioned(
                                  top: 10,
                                  right: 10,
                                  child: Container(
                                    decoration: const BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: ColorConstance.cDefaultColor),
                                    child: const Icon(
                                      Icons.done_outline_rounded,
                                      size: 20,
                                      color: Colors.white,
                                    ),
                                  ),
                                )
                              ] else ...[
                                const SizedBox()
                              ]
                            ]
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 20,
          horizontal: 10,
        ),
        child: Align(
          alignment: Alignment.bottomCenter,
          child: CustomButton(
            width: media.width,
            text: "Done",
            backgroundColor: state.interstMovieList.length != 3
                ? const Color.fromARGB(255, 165, 82, 80)
                : null,
            onPressed: state.interstMovieList.length != 3
                ? () {}
                : () {
                    prefs
                        .setString("InterstingMovie",
                            state.interstMovieList.toString())
                        .whenComplete(() {
                      print(
                          "Intersting Movie List => ${prefs.getString("InterstingMovie")}");

                      AnimationRoute.fadeRoutePushAndRemoveUntil(
                        context,
                        pageRoute: const BottomNavigationBarScreens(),
                      );
                    });
                  },
          ),
        ),
      )
    ]);
  }
}
